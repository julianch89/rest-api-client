package com.jchierichetti.restapilibrary.service;


import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.jchierichetti.restapilibrary.RequestServiceHelper;
import com.jchierichetti.restapilibrary.processor.BaseProcessor;
import com.jchierichetti.restapilibrary.processor.ProcessorCallback;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.response.ResponseMessage;
import com.jchierichetti.restapilibrary.response.RestMethodResult;


public class RequestIntentService extends IntentService{
	
	public static final String PACKAGE_NAME = "com.jchierichetti.restapilibrary";
	
	/** Extra: Unique request type identifier, e.g. Login, SignUp, etc... */
	public static final String API_REQUEST_TYPE_ID = PACKAGE_NAME + "API_REQUEST_TYPE";

	/** Extra: Callback for request result (ResultReceiver) */
	public static final String SERVICE_CALLBACK =  PACKAGE_NAME + "SERVICE_CALLBACK";

	/** Extra: Intent that triggered the request */
	public static final String ORIGINAL_INTENT =  PACKAGE_NAME + "ORIGINAL_INTENT";

	/** Extra: Error Message */
	public static final String EXTRA_ERROR_MESSAGE =  PACKAGE_NAME + "EXTRA_ERROR_MESSAGE";

	public static final String EXTRA_ORIGNAL_RESPONSE = PACKAGE_NAME + "EXTRA_ORIGINAL_RESPONSE";

	/** Extra: Parameters bundle */
	public static final String EXTRA_PARAMETERS_BUNDLE =  PACKAGE_NAME + "EXTRA_PARAMETERS_BUNDLE";

	public static final int REQUEST_FAIL = 0;
	public static final int REQUEST_SUCCESS = 1;
	public static final int REQUEST_TIME_OUT = 2;
	
	/** Callback to send request result */
	private ResultReceiver mCallback;

	/** Intent that triggered the request */
	private Intent mOriginalRequestIntent;
	
	private BaseProcessor mBaseProcessor;

	
	public RequestIntentService()
	{
		super("RequestIntentService");
		mBaseProcessor = new BaseProcessor(RequestServiceHelper.getContext());
		
	}

	@Override
	protected void onHandleIntent(Intent requestIntent)
	{
		mOriginalRequestIntent = requestIntent;
		mCallback = requestIntent.getParcelableExtra(RequestIntentService.SERVICE_CALLBACK);

		String method = requestIntent.getStringExtra(RequestIntentService.API_REQUEST_TYPE_ID);
		Bundle parameters = requestIntent.getBundleExtra(RequestIntentService.EXTRA_PARAMETERS_BUNDLE);
		long requestId =  requestIntent.getLongExtra(RequestServiceHelper.EXTRA_REQUEST_TRANSACTION_ID,-1 );
		RequestConfiguration.RequestType requestType = RequestServiceHelper.getRequestConfiguration().getRequestMap().get(method);

		mBaseProcessor.queueRequest(requestType, parameters, makeProcessorCallback(),requestId);
	}
	
	/*** Notify the caller via callback, using provided ResultReceiver */
	private ProcessorCallback makeProcessorCallback()
	{
		ProcessorCallback callback = new ProcessorCallback()
		{
			@Override
			public void send(RestMethodResult result)
			{
				if (mCallback != null)
					mCallback.send(result.getCode(), getOriginalIntentBundle(result.getResponseMessage()));
			}
		};
		return callback;
	}

	/** Returns a bundle with the intent who originally started the request */
	protected Bundle getOriginalIntentBundle(ResponseMessage responseMessage)
	{
		Bundle originalRequest = new Bundle();
		originalRequest.putParcelable(ORIGINAL_INTENT, mOriginalRequestIntent);
		originalRequest.putParcelable(EXTRA_ERROR_MESSAGE, responseMessage);
		return originalRequest;
	}

}
