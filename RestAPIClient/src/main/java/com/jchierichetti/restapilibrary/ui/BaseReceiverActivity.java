package com.jchierichetti.restapilibrary.ui;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.jchierichetti.restapilibrary.RequestServiceHelper;
import com.jchierichetti.restapilibrary.util.RestConstants;

public abstract class BaseReceiverActivity extends FragmentActivity implements RequestResultReceiver.Receiver {

	protected RequestResultReceiver mRequestReceiver;
	protected List<Long> mRequestList;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mRequestReceiver = new RequestResultReceiver(this);
		mRequestList = new ArrayList<Long>();
	}

	@Override
	protected void onPause()
	{
		mRequestReceiver.setReceiver(null);
		super.onPause();
		
	}
	
	protected boolean isValidRequest(long requestId)
	{
		return mRequestList.contains(requestId);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		mRequestReceiver.setReceiver(this);
		for (Long requestId : mRequestList)
		{
			String status = RequestServiceHelper.mRequestStatusHandler.getRequestStatus(requestId);
			switch (status)
			{
				case(RequestServiceHelper.RequestStatusHandler.DELIVERED_STATUS):
					return;
				case(RequestServiceHelper.RequestStatusHandler.RECEIVED_STATUS):
					int code = RequestServiceHelper.mRequestStatusHandler.getRequestCode(requestId);
					if(code == RestConstants.HttpStatus.OK_200.getCode())
						onRequestSuccess(requestId);
					onRequestFinished(requestId);

			}

		}

	}

}
