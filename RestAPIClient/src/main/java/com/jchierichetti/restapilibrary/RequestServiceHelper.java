package com.jchierichetti.restapilibrary;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.android.volley.Request;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.response.ResponseMessage;
import com.jchierichetti.restapilibrary.service.RequestIntentService;


import org.json.JSONObject;

import java.util.UUID;

public class RequestServiceHelper {

	/** Id used to broadcast request result */
	public static final String ACTION_REQUEST_RESULT = "ACTION_REQUEST_RESULT";

	/** Extra: Auto-generated unique request identifier */
	public static final String EXTRA_REQUEST_TRANSACTION_ID = "EXTRA_REQUEST_TRANSACTION_ID";

	/** Extra: Request result code, e.g. 200 (Ok), 400 (Bad Request) */
	public static final String EXTRA_RESULT_CODE = "EXTRA_RESULT_CODE";

	/** Extra: Request result message */
	public static final String EXTRA_RESULT_MSG = "EXTRA_RESULT_MSG";

	/** Auto-generated unique request identifier */
	private static long generateRequestID()
	{
		long random = UUID.randomUUID().getLeastSignificantBits();
		if(random < 0 ) random = random * -1;
		return random;
	}
    /** Application interface to implements Volley queue methods */
    public interface ApplicationWithVolley
    {
        public <T> void addToRequestQueue(Request<T> req, String tag);
        public <T> void addToRequestQueue(Request<T> req);
		public void cancelPendingRequests(Object tag);

    }


    /**  Context used to start service and broadcast */
	private static Context mContext;
    /**  Application instance to add request on the queue */
    private static ApplicationWithVolley mApplication;
    /** Request configuration with the request type list */
    private static RequestConfiguration mRequestConfiguration;

    public static RequestStatusHandler mRequestStatusHandler;

    public interface RequestStatusHandler
    {
        public static final String DELIVERED_STATUS = "delivered";
        public static final String RECEIVED_STATUS = "received";

        public void setRequestStatusDelivered(long requestId);
        public void setRequestStatusReceived(long requestId, int httpCode,String msg, String response);
        public String getRequestStatus(long requestId);
        public int getRequestCode(long requestId);
    }

    public static void setRequestStatusHandler(RequestStatusHandler requestStatusHandler)
    {
        mRequestStatusHandler = requestStatusHandler;
    }


    /** Set the request configuration with all the request types*/
    public static void setRequestConfiguration(RequestConfiguration requestConfiguration)
    {
        mRequestConfiguration = requestConfiguration;
    }

	/** Set the context used for Broadcast and SErvices */
	public static void setContext(Context context)
	{
		mContext = context;
	}

    public static Context getContext()
    {
        return mContext;
    }

    /**  Set the application instance with the volley request queue */
    public static void setApplication(ApplicationWithVolley application)
    {
        mApplication = application;
    }

    public static ApplicationWithVolley getApplicationInstance()
    {
        return mApplication;
    }

    public static RequestConfiguration getRequestConfiguration()
    {
        return mRequestConfiguration;
    }

	/** Callback to handle Request result */
	private static ResultReceiver mServiceCallback = new ResultReceiver(null) {
		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData)
		{
			handleRequestResponse(resultCode, resultData);
		}
	};

	/**
	 * Receives request result and Broadcast result information
	 * 
	 * @param resultCode
	 *            Request result (HTTP status code)
	 * 
	 * @param resultData
	 *            Bundle with extra information about request result
	 * */
	private static void handleRequestResponse(int resultCode, Bundle resultData)
	{
		Intent originalIntent = (Intent) resultData.getParcelable(RequestIntentService.ORIGINAL_INTENT);
		ResponseMessage msg = resultData.getParcelable(RequestIntentService.EXTRA_ERROR_MESSAGE);

		if (originalIntent != null)
		{
			long requestId = originalIntent.getLongExtra(EXTRA_REQUEST_TRANSACTION_ID, 0);

			Intent resultBroadcast = new Intent(ACTION_REQUEST_RESULT);
			resultBroadcast.putExtra(EXTRA_REQUEST_TRANSACTION_ID, requestId);
			resultBroadcast.putExtra(EXTRA_RESULT_CODE, resultCode);
			resultBroadcast.putExtra(EXTRA_RESULT_MSG, msg);

			mContext.sendBroadcast(resultBroadcast);
		}
	}

	/**
	 * Creates an Intent with request type and parameters and send it to
	 * RequestIntentService.
	 * 
	 * @param requestType
	 *            Unique identifier for API request
	 * 
	 * @param parameters
	 *            Parameters needed to make API request
	 * 
	 * @return Unique ID to identify the transaction
	 * */
	public static long requestToService(String requestType, Bundle parameters)
	{
        if(mContext == null)
            throw new RuntimeException("Context is null, it used to starts the service and sends the broadcast. ");
        if(mApplication == null)
            throw new RuntimeException("Application is null, it used to add the request to the queue ");
		long requestId = generateRequestID();

		Intent intent = new Intent(mContext, RequestIntentService.class);
		intent.putExtra(RequestIntentService.API_REQUEST_TYPE_ID, requestType);
		intent.putExtra(RequestIntentService.SERVICE_CALLBACK, mServiceCallback);
		intent.putExtra(RequestIntentService.EXTRA_PARAMETERS_BUNDLE, parameters);
		intent.putExtra(EXTRA_REQUEST_TRANSACTION_ID, requestId);

		mContext.startService(intent);
		return requestId;
	}

	
}
