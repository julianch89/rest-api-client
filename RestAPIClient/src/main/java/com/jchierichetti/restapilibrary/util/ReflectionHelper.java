package com.jchierichetti.restapilibrary.util;

import android.util.Log;

import java.lang.reflect.Constructor;

public class ReflectionHelper
{
	private static final String TAG = ReflectionHelper.class.getSimpleName();

	@SuppressWarnings("unchecked")
	public static <T> T createInstance(Class<?> clazz)
	{
		String msg = null;
		Object newInstance = null;
		try
		{
			Constructor<?> constructor = clazz.getConstructors()[0];
			newInstance = constructor.newInstance();
		} catch (IllegalArgumentException e)
		{
			msg = e.getMessage();
		} catch (Exception e)
		{
			msg = "ReflectiveOperationException";
		} finally
		{
			if (msg != null)
			{
				Log.e(TAG, "error instantiating typee " + clazz.getClass().getSimpleName() + "\n" + msg);
				newInstance = null;
			}
		}
		return (T) newInstance;
	}

}
