package com.jchierichetti.restapilibrary.request.base;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

/**
 * Created by admin on 7/10/2015.
 */
public abstract class RequestConfiguration {


    private final HashMap<String,RequestType> mRequestTypeList;

    public RequestConfiguration(HashMap<String,RequestType> requestList)
    {
        mRequestTypeList = requestList;
    }

    public HashMap<String,RequestType> getRequestMap()
    {
        return mRequestTypeList;
    }


    public static class RequestType
    {
        public RequestType(Class<?> requestClass,Type responseClass, boolean handleResponse)
        {
            mRequestClass = requestClass;
            mResponseClass = responseClass;
            mHandleResponse = handleResponse;
        }

        private Class<?> mRequestClass;
        private Type mResponseClass;
        private boolean mHandleResponse;


        /** Class that handle the request */
        public Class<?> getRequestClass()
        {
            return mRequestClass;
        }

        /** Class that handle the request's response */
        public Type getResponseClass()
        {
            return mResponseClass;
        }

        /** Returns true if the request needs to handle the response with a processor */
        public boolean needsHandleResponse()
        {
            return mHandleResponse;
        }

    }

}
