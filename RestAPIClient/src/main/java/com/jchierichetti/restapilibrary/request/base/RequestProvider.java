package com.jchierichetti.restapilibrary.request.base;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;

import org.json.JSONObject;

import java.lang.reflect.Constructor;

public class RequestProvider
{
	protected static final String TAG = RequestProvider.class.getSimpleName();

	/**
	 * Creates an instance of a specific RequestType.
	 * 
	 * @param requestType
	 *            Request type (e.g. Login, Logout, SignUp)
	 * 
	 * @param parameters
	 *            Bundle with optional and required parameters
	 * 
	 * @param listener
	 *            Listener to handle request successful response
	 * 
	 * @param errorListener
	 *            Listener to handle request error response
	 * */
	public static Request<?> createRequest(RequestConfiguration.RequestType requestType, Bundle parameters,
			Listener<JSONObject> listener, ErrorListener errorListener)
	{
		String msg = null;
		Request<?> request = null;
		try
		{
			Constructor<?> constructor = requestType.getRequestClass().getConstructor(Bundle.class, Listener.class,
					ErrorListener.class);
			request = (Request<?>) constructor.newInstance(parameters, listener, errorListener);
		} catch (IllegalArgumentException e)
		{
			msg = e.getMessage();
		} catch (Exception e)
		{
			msg = "ReflectiveOperationException";
		} finally
		{
			if (msg != null)
			{
				Log.e(TAG, "error instantiating request of type " + requestType + "\n" + msg);
				request = null;
			}
		}
		return request;
	}
}
