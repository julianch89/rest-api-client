package com.jchierichetti.restapilibrary.request;

import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

/** Class used to simplify parameters traffic */
public class ParametersBundle
{
	protected static final String TAG = ParametersBundle.class.getSimpleName();

	/**
	 * Create a Bundle with all the key/values received by parameter
	 * 
	 * @param parameters
	 *            Key/values representing parameters name and value
	 * 
	 * @return Bundle with all key/values received by parameter
	 * */
	public static Bundle fromMap(Map<String, String> parameters)
	{
		Bundle b = new Bundle();
		for (String parameter : parameters.keySet())
			b.putString(parameter, parameters.get(parameter));
		return b;
	}

	/**
	 * Retrieves values from bundle and creates a Map with of parameters name and value
	 * 
	 * @param parametersBundle
	 *            Bundle with all key/values representing a set of parameters
	 * 
	 * @return Map with parameters name and value
	 */
	public static Map<String, String> toMap(Bundle parametersBundle)
	{
		Map<String, String> parameters = new HashMap<String, String>();

		if (parametersBundle != null)
			for (String key : parametersBundle.keySet())
				parameters.put(key, parametersBundle.getString(key));

		return parameters;
	}

}
