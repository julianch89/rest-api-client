package com.jchierichetti.restapilibrary.response;


public class RestMethodResult
{
	private int  mCode;
	private ResponseMessage mResponseMessage;

	public RestMethodResult(int code, ResponseMessage statusMsg)
	{
		super();
		mCode = code;
		mResponseMessage = statusMsg;
		
	}
	
	public int getCode()
	{
		return mCode;
	}

	public ResponseMessage getResponseMessage()
	{
		return mResponseMessage;
	}


}
