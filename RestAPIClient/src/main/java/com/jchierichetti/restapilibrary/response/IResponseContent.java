package com.jchierichetti.restapilibrary.response;

import java.lang.reflect.Type;

public interface IResponseContent
{
	public Type getResourceType();
}
