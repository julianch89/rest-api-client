package com.jchierichetti.restapilibrary.response;


import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.jchierichetti.restapilibrary.util.RestConstants;


public class VolleyErrorHelper {
	/**
	 * Returns appropriate message which is to be displayed to the user against
	 * the specified error object.
	 * 
	 * @param error
	 * @param context
	 * @return
	 */
	public static ResponseMessage getMessage(Object error, Context context)
	{
		if (error instanceof TimeoutError)
			return ResponseMessage.timeoutResponse();
		else if (isServerProblem(error))
			return handleServerError(error, context);
		else if (isNetworkProblem(error))
			return ResponseMessage.noIntenertResponse();
		return ResponseMessage.unknownErrorResponse();
	}

	/**
	 * Determines whether the error is related to network
	 * 
	 * @param error
	 * @return
	 */
	private static boolean isNetworkProblem(Object error)
	{
		return (error instanceof NetworkError) || (error instanceof NoConnectionError);
	}

	/**
	 * Determines whether the error is related to server
	 * 
	 * @param error
	 * @return
	 */
	private static boolean isServerProblem(Object error)
	{
		return (error instanceof ServerError) || (error instanceof AuthFailureError);
	}

	/**
	 * Handles the server error, tries to determine whether to show a stock
	 * message or to show a message retrieved from the server.
	 * 
	 * @param err
	 * @param context
	 * @return
	 */
	private static ResponseMessage handleServerError(Object err, Context context)
	{
		VolleyError error = (VolleyError) err;

		NetworkResponse response = error.networkResponse;

		ResponseMessage result = ResponseMessage.unknownErrorResponse();

		if (response == null)
			return result;

		switch (RestConstants.HttpStatus.from(response.statusCode))
		{
		case LENGTH_REQUIRED_411:
		case PRECONDITION_FAILED_412:
		case BAD_REQUEST_400:
			result = new ResponseMessage(String.valueOf(response.statusCode),response.data.toString());
			break;
		case REQUEST_TIMEOUT_408:
			result = ResponseMessage.timeoutResponse();
			break;
		case UNAUTHORIZED_401:
			result = ResponseMessage.errorWithHTTPStatus("401 unauthorized");
			break;
		case FORBIDDEN_403:
			result = ResponseMessage.errorWithHTTPStatus("403 forbidden");
			break;
		case NOT_FOUND_404:
			result = ResponseMessage.errorWithHTTPStatus("404 not found");
			break;

		case INTERNAL_SERVER_ERROR_500:
			result = ResponseMessage.errorWithHTTPStatus("500 internal server error");
			break;
		default:
			break;
		}
		return result;
	}

}
