package com.jchierichetti.restapilibrary.response;

import android.os.Parcel;
import android.os.Parcelable;


/** Class to handle a single response message associated to API requests */
public class ResponseMessage implements Parcelable
{
	private String mResponseCode;
	private String mResponseLabel;

	public ResponseMessage(String code, String label)
	{
		mResponseCode = code;
		mResponseLabel = label;
	}


	/** Unique identifier for API responses */
	public String getResponseCode()
	{
		return mResponseCode;
	}

	/** Message associated to response code, already localized */
	public String getResponseLabel()
	{
		return mResponseLabel;
	}


	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeStringArray(new String[] { mResponseCode, mResponseLabel });
	}

	public ResponseMessage(Parcel in)
	{
		String[] data = new String[2];

		in.readStringArray(data);
		mResponseCode = data[0];
		mResponseLabel = data[1];
	}

	public static final Creator<ResponseMessage> CREATOR = new Creator<ResponseMessage>()
	{
		public ResponseMessage createFromParcel(Parcel in)
		{
			return new ResponseMessage(in);
		}

		public ResponseMessage[] newArray(int size)
		{
			return new ResponseMessage[size];
		}
	};

	public static ResponseMessage noIntenertResponse()
	{
		return new ResponseMessage("Connection problems", "No internet connection"); // TODO [Resources] resource strings
	}

	public static ResponseMessage timeoutResponse()
	{
		return new ResponseMessage("Timeout", "Request timed out");// TODO [Resources] resource strings
	}

	public static ResponseMessage unknownErrorResponse()
	{
		return new ResponseMessage("Unknownerror", "Unknown error");// TODO [Resources] resource strings
	}

	public static ResponseMessage undefinedRequestResponse()
	{
		return new ResponseMessage("UndefinedRequest", "Undefined Request");// TODO [Resources] resource strings
	}

	public static ResponseMessage jsonExceptionResponse()
	{
		return new ResponseMessage("JsonException", "JSON parsing exception");// TODO [Resources] resource strings
	}

	public static ResponseMessage errorWithHTTPStatus(String message)
	{
		return new ResponseMessage("HttpError", message);// TODO [Resources] resource strings
	}
}
