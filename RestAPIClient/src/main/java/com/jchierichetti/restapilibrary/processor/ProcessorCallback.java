package com.jchierichetti.restapilibrary.processor;


import com.jchierichetti.restapilibrary.response.RestMethodResult;

public interface ProcessorCallback
{
	void send(RestMethodResult result);
}
