package com.jchierichetti.restapilibrary.processor;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.jchierichetti.restapilibrary.RequestServiceHelper;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.request.base.RequestProvider;
import com.jchierichetti.restapilibrary.response.IResponseContent;
import com.jchierichetti.restapilibrary.response.ResponseMessage;
import com.jchierichetti.restapilibrary.response.RestMethodResult;
import com.jchierichetti.restapilibrary.response.VolleyErrorHelper;
import com.jchierichetti.restapilibrary.service.RequestIntentService;
import com.jchierichetti.restapilibrary.util.ReflectionHelper;
import com.jchierichetti.restapilibrary.util.RestConstants;


import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;


public class BaseProcessor {
	protected static final String TAG = BaseProcessor.class.getSimpleName();

	public static Map<Type, Class<? extends IBaseProcessor>> processorsMapping = new HashMap<Type, Class<? extends IBaseProcessor>>();


    public void addProccesor(Type type,Class<? extends IBaseProcessor> processor)
    {
        processorsMapping.put(type,processor);
    }


	protected Context mContext;

	public BaseProcessor(Context context)
	{
		mContext = context;
	}

	public void queueRequest(RequestConfiguration.RequestType request, Bundle parameters, final ProcessorCallback callback, final long requestId)
	{
		Request<?> req = RequestProvider.createRequest(request, parameters, getResponseListener(request, callback, requestId), getErrorListener(callback, requestId));
		RequestServiceHelper.mRequestStatusHandler.setRequestStatusDelivered(requestId);

		RequestServiceHelper.getApplicationInstance().addToRequestQueue(req);
	}

	private Response.ErrorListener getErrorListener(final ProcessorCallback callback, final long requestId)
	{
		return new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error)
			{
				// TODO check the request status
				if (error.networkResponse == null) {
					Log.e(TAG, "network response is null. " + error.getMessage());
					callback.send(new RestMethodResult(RequestIntentService.REQUEST_FAIL, ResponseMessage.noIntenertResponse()));
                    RequestServiceHelper.mRequestStatusHandler.setRequestStatusReceived(requestId, RestConstants.HttpStatus.NOT_FOUND_404.getCode(), "network response is null", "");
				} else {
					ResponseMessage response = VolleyErrorHelper.getMessage(error, mContext);
					callback.send(new RestMethodResult(RequestIntentService.REQUEST_FAIL, response));
                    RequestServiceHelper.mRequestStatusHandler.setRequestStatusReceived(requestId, error.networkResponse.statusCode, response.getResponseLabel(),"");
				}
			}
		};
	}

	private Response.Listener<JSONObject> getResponseListener(final RequestConfiguration.RequestType request, final ProcessorCallback callback, final long requestId)
	{
		return new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response)
			{
				if(request.needsHandleResponse())
                	RequestServiceHelper.mRequestStatusHandler.setRequestStatusReceived(requestId, RestConstants.HttpStatus.OK_200.getCode(), "","");
				else
					RequestServiceHelper.mRequestStatusHandler.setRequestStatusReceived(requestId, RestConstants.HttpStatus.OK_200.getCode(),"",response.toString());
				if (request.getResponseClass() != null) {
					IResponseContent resourceResponse = new Gson().fromJson(response.toString(), request.getResponseClass());
					if(request.needsHandleResponse()) {
						IBaseProcessor processor = ReflectionHelper.createInstance(processorsMapping.get(resourceResponse.getResourceType()));
						processor.handleResponse(mContext, request, resourceResponse);
					}
				}
				callback.send(new RestMethodResult(RequestIntentService.REQUEST_SUCCESS, null));
			}
		};
	}








}
