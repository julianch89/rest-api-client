package com.jchierichetti.restapilibrary.processor;


import android.content.Context;

import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.response.IResponseContent;


public interface IBaseProcessor
{
	public void handleResponse(Context context, RequestConfiguration.RequestType requestId, IResponseContent responseContent);
}
