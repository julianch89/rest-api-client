package com.jchierichetti.restapptest;

/**
 * Created by admin on 7/13/2015.
 */
public class ResponseContent {

    private String mTime;
    private String mDate;


    public ResponseContent(String time, String date)
    {
        mDate = date;
        mTime = time;
    }

    public String getTime()
    {
        return mTime;
    }
    public String getDate()
    {
        return mDate;
    }

}
