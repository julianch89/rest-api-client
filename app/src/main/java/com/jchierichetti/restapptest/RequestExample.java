package com.jchierichetti.restapptest;

import android.os.Bundle;

import com.android.volley.Response;
import com.jchierichetti.restapilibrary.request.base.BaseJsonRequest;

import org.json.JSONObject;

/**
 * Created by admin on 7/13/2015.
 */
public class RequestExample extends BaseJsonRequest {

    private static final String URL = "http://date.jsontest.com/";

    public RequestExample(Bundle parameters, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener)
    {
        super(Method.GET,URL,parameters,listener,errorListener);
    }


    @Override
    public int getMethod() {
        return Method.GET;
    }
}
