package com.jchierichetti.restapptest;

import android.content.Context;

import com.jchierichetti.restapilibrary.processor.IBaseProcessor;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.response.IResponseContent;

/**
 * Created by admin on 7/13/2015.
 */
public class ProcessorExample implements IBaseProcessor {
    @Override
    public void handleResponse(Context context, RequestConfiguration.RequestType requestId, IResponseContent responseContent) {

    }
}
