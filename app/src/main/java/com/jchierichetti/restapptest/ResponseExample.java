package com.jchierichetti.restapptest;

import com.google.gson.annotations.SerializedName;
import com.jchierichetti.restapilibrary.response.IResponseContent;

import java.lang.reflect.Type;

/**
 * Created by admin on 7/13/2015.
 */
public class ResponseExample implements IResponseContent {

    @SerializedName("time")
    private String mTime;
    @SerializedName("date")
    private String mDate;


    @Override
    public Type getResourceType() {
        return ResponseContent.class;
    }
}
