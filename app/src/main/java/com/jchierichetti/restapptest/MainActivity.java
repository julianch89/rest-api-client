package com.jchierichetti.restapptest;

import android.database.Cursor;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.jchierichetti.restapilibrary.RequestServiceHelper;
import com.jchierichetti.restapilibrary.processor.BaseProcessor;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;
import com.jchierichetti.restapilibrary.response.ResponseMessage;
import com.jchierichetti.restapilibrary.ui.BaseReceiverActivity;

import org.json.JSONObject;


public class MainActivity extends BaseReceiverActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RequestServiceHelper.setApplication(RestApiApp.getInstance());
        RequestServiceHelper.setContext(RestApiApp.getInstance().getApplicationContext());
        RequestServiceHelper.setRequestStatusHandler(new RequestServiceHelper.RequestStatusHandler() {


            @Override
            public void setRequestStatusDelivered(long requestId) {

            }

            @Override
            public void setRequestStatusReceived(long requestId, int httpCode, String msg, String response) {

            }

            @Override
            public String getRequestStatus(long requestId) {
                return null;
            }

            @Override
            public int getRequestCode(long requestId) {
                return 0;
            }
        });
        RequestServiceHelper.setRequestConfiguration(new RequestListConfiguration());
        BaseProcessor.processorsMapping.put(ResponseContent.class, ProcessorExample.class);



        RequestServiceHelper.requestToService(RequestListConfiguration.REQUEST_EXAMPLE,new Bundle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestSuccess(long requestId) {

    }

    @Override
    public void onRequestError(long requestId, int resultCode, ResponseMessage msg) {

    }

    @Override
    public void onRequestFinished(long requestId) {

    }
}
