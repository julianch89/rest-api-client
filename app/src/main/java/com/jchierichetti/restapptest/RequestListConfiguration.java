package com.jchierichetti.restapptest;


import com.google.gson.reflect.TypeToken;
import com.jchierichetti.restapilibrary.request.base.RequestConfiguration;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 7/13/2015.
 */
public class RequestListConfiguration extends RequestConfiguration{


    public static final String REQUEST_EXAMPLE = "RequestExample";

    public static final HashMap<String,RequestType> mRequestList = new HashMap<String,RequestType>(){{
        put(REQUEST_EXAMPLE,new RequestType(RequestExample.class,new TypeToken<ResponseExample>(){}.getType(),false));
    }};

    public RequestListConfiguration()
    {
        super(mRequestList);
    }

}
